from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import tkinter

def receive():
    while True:
        msg = client.recv(buffersize).decode("utf8")
        msg_list.insert(tkinter.END, msg)
        msg_list.see(tkinter.END)

def send(event=None):
    msg = my_msg.get()
    my_msg.set("")
    client.send(bytes(msg, "utf8"))
    if msg == "exit()":
        client.close()
        top.quit()

def on_closing(event=None):
    my_msg.set("exit()")
    send()

top = tkinter.Tk()
top.title("SocketChat Client")
msg_frame = tkinter.Frame(top)
my_msg = tkinter.StringVar()
my_msg.set("")
scrollbar = tkinter.Scrollbar(msg_frame)
msg_list = tkinter.Text(msg_frame, height=20, width=50, yscrollcommand=scrollbar.set)
scrollbar.grid(row=0, column=1, sticky="W", in_=msg_frame)
msg_list.grid(row=0, column=0, sticky="W", in_=msg_frame, padx=2, pady=2)
msg_list.grid()
msg_frame.grid()
entry_field = tkinter.Entry(top, textvariable=my_msg, width=40)
entry_field.bind("<Return>", send)
entry_field.grid(row=2, column=0, sticky="NW")
entry_field.focus_set()
send_button = tkinter.Button(top, text="↵", command=send, width=10)
send_button.grid(row=2, column=0, sticky="E")
top.protocol("WM_DELETE_WINDOW", on_closing)

host = "127.0.0.1"
port = 55000
buffersize = 1024
address = (host, int(port))
client = socket(AF_INET, SOCK_STREAM)
client.connect(address)
receive_thread = Thread(target=receive)
receive_thread.start()
tkinter.mainloop()
