from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread

def accept_incoming_connections():
    while True:
        client, client_address = server.accept()
        client.send(bytes("Type your username\n", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()

def handle_client(client):
    name = client.recv(buffersize).decode("utf8")
    prefix = "(%s@%s:%s): " % (name, str(client.getpeername()[0]), str(client.getpeername()[1]))
    welcome = "To quit, type exit()\n"
    client.send(bytes(welcome, "utf8"))
    broadcast_save(bytes("joined the chat!", "utf8"), prefix)
    load_history(client)
    clients[client] = name
    while True:
        msg = client.recv(buffersize)
        if msg != bytes("exit()", "utf8"):
            broadcast_save(msg, prefix)
        else:
            client.send(bytes("exit()", "utf8"))
            client.close()
            del clients[client]
            broadcast_save(bytes("left the chat.", "utf8"), prefix)
            break

def broadcast_save(msg, prefix=""):
    history_file = open("history.file", "a")
    for sock in clients:
        sock.send(bytes(prefix, "utf8") + msg + bytes("\n", "utf8"))
    history_file.write(prefix + msg.decode("utf8") + "\n")
    history_file.close()

def load_history(client):
    history_file = open("history.file", "r")
    for l in history_file:
        client.send(bytes(l, "utf8"))
    history_file.close()

clients = {}
addresses = {}
host = "127.0.0.1"
port = 55000
buffersize = 1024
address = (host, port)
server = socket(AF_INET, SOCK_STREAM)
server.bind(address)

if __name__ == "__main__":
    server.listen()
    print("Waiting for connection...")
    accept_thread = Thread(target=accept_incoming_connections)
    accept_thread.start()
    accept_thread.join()
    server.close()
